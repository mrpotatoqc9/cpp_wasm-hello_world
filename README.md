# Cpp_wasm-hello_world



## Getting started

ubuntu 22 requirements
```
sudo add-apt-repository universe
sudo apt install qt6-base-dev cmake
cd ~/
git clone https://github.com/emscripten-core/emsdk.git
cd emsdk/
./emsdk install latest
./emsdk activate latest
source ~/emsdk/emsdk_env.sh
em++ --version
```

### Compile
```
cd  ~/
git clone https://gitlab.com/mrpotatoqc9/cpp_wasm-hello_world.git
cd cpp_wasm-hello_world
mkdir build
cd build
emcmake cmake ..
make
```

### Run WebPage

```
emrun MyProject.html
```
